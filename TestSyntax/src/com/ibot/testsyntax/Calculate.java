package com.ibot.testsyntax;

import java.util.Scanner;

public class Calculate {

	public static void main(String[] args) {
		// User input
		int totalAllowedNumber = 9;
		int[] inputNumbers = new int[totalAllowedNumber];
		int index = 0;
		Scanner scan= new Scanner(System.in);
		while (totalAllowedNumber != 0) {
		    System.out.print("Enter number in position " + (index+1) + ":");
		    // System.out.println(Integer.parseInt(scan.nextLine()));
		    inputNumbers[index] = Integer.parseInt(scan.nextLine());
			totalAllowedNumber--;
			index++;
		}
	    
		// Calculation 
		// int[] numbers = { 3, 2, 5, 6, 8, 0, 7, 7, 4 };
		int total = inputNumbers[0];
		for (int i = 0; i < inputNumbers.length; i++) {
			// ensure next number is not out of bound
			if (i+1 < inputNumbers.length) {
				total = calculate(total, inputNumbers[i + 1]);
				
				if (i%2 == 0) {
					System.out.println(total);
				}
			}
		}
	}

	public static int calculate(int total, int newNumber) {
		total += newNumber;
		return total;
	}
}
