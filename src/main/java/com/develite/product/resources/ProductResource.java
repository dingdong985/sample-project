package com.develite.product.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.develite.product.dto.EncryptedRequestDTO;
import com.develite.product.dto.EncryptedResponseDTO;
import com.develite.product.dto.ProductRequestDTO;
import com.develite.product.dto.ProductResponseDTO;
import com.develite.product.services.ProductService;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1/products")
@Slf4j
public class ProductResource {

	@Autowired
	private ProductService productService;

	@ApiOperation(value = "List all products")
	@GetMapping("")
	public ResponseEntity<List<ProductResponseDTO>> getAllProducts() {

		return ResponseEntity.ok(productService.getAllProducts());
	}

	@ApiOperation(value = "View product by given identifier", response = ProductResponseDTO.class)
	@GetMapping("{id}")
	public ResponseEntity<ProductResponseDTO> getProductById(@PathVariable("id") Long id) {
		return ResponseEntity.ok(productService.getProductById(id));
	}

	@ApiOperation(value = "View product by given identifier (encrypted)", response = ProductResponseDTO.class)
	@GetMapping("{id}/encrypted")
	public ResponseEntity<EncryptedResponseDTO> getProductByIdEncrypted(@PathVariable("id") Long id) {
		String encrytedResponse = productService.getProductByIdEncrypted(id);
		EncryptedResponseDTO responseData = EncryptedResponseDTO.builder().responseData(encrytedResponse).build();
		return ResponseEntity.ok(responseData);
	}

	@ApiOperation(value = "Create product")
	@PostMapping()
	public ResponseEntity<?> createProduct(@RequestBody ProductRequestDTO productDTO) {
		productService.createProduct(productDTO);
		return ResponseEntity.noContent().build();
	}

	@ApiOperation(value = "Create product (Encrypted)")
	@PostMapping("/encrypted")
	public ResponseEntity<?> createProductEncrypted(@RequestBody EncryptedRequestDTO encryptedRequestDTO) {
		productService.createProductWithEncrypted(encryptedRequestDTO);
		return ResponseEntity.noContent().build();
	}

	@ApiOperation(value = "Update product by given identifier")
	@PostMapping("{id}")
	public ResponseEntity<?> updateProductById(@PathVariable("id") Long id, @RequestBody ProductRequestDTO productDTO) {
		productService.updateProduct(id, productDTO);
		return ResponseEntity.noContent().build();
	}

	@ApiOperation(value = "Delete product by given identifier")
	@PostMapping("{id}/delete")
	public ResponseEntity<?> deleteProductById(@PathVariable("id") Long id) {
		productService.deleteProduct(id);
		return ResponseEntity.noContent().build();
	}

}
