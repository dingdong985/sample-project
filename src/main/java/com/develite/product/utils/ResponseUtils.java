package com.develite.product.utils;

import org.springframework.http.ResponseEntity;

public class ResponseUtils {

	public static ResponseEntity<?> emptyOkResponse() {
		return ResponseEntity.noContent().build();
	}
}
