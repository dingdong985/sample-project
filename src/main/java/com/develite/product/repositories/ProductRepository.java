package com.develite.product.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.develite.product.model.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

}
