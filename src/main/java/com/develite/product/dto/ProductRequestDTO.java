package com.develite.product.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ProductRequestDTO extends BaseProductDTO {

	@ApiModelProperty(required = true)
	private String name;
	
	@ApiModelProperty(required = true)
	private Float price;
}
