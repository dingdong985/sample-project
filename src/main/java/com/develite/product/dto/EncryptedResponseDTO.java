package com.develite.product.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EncryptedResponseDTO {

	private String responseData;
}
