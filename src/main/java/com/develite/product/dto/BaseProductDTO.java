package com.develite.product.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseProductDTO {

	private String name;
	private Float price;
}
