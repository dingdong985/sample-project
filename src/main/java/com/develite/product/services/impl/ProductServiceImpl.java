package com.develite.product.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.develite.product.dto.EncryptedRequestDTO;
import com.develite.product.dto.ProductRequestDTO;
import com.develite.product.dto.ProductResponseDTO;
import com.develite.product.exceptions.InvalidRequestException;
import com.develite.product.exceptions.NotFoundException;
import com.develite.product.model.Product;
import com.develite.product.repositories.ProductRepository;
import com.develite.product.services.ProductService;
import com.develite.product.utils.AesUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

	@Value("${app.secret-key}")
	private String secretKeyValue;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public List<ProductResponseDTO> getAllProducts() {
		Iterable<Product> products = productRepository.findAll();

		List<ProductResponseDTO> productDTOList = new ArrayList<>();
		for (Product product : products) {
			ProductResponseDTO productDTO = modelMapper.map(product, ProductResponseDTO.class);
			productDTOList.add(productDTO);
		}

		return productDTOList;
	}

	@Override
	public ProductResponseDTO getProductById(Long id) {
		Product product = productRepository.findById(id)
				.orElseThrow(() -> new NotFoundException("Product with id = " + id + " not found"));
		ProductResponseDTO productDTO = modelMapper.map(product, ProductResponseDTO.class);

		return productDTO;
	}

	@Override
	public void createProduct(ProductRequestDTO productDTO) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			String json = objectMapper.writeValueAsString(productDTO);
			System.out.println("ResultingJSONstring = " + json);
			
			System.out.println("AES req body" + AesUtils.encrypt(json, secretKeyValue));
			
		} catch (JsonProcessingException e) {
			throw new RuntimeException("Error prossing result ");
		}
		
		
		if (productDTO == null) {
			throw new InvalidRequestException("Request body not found");
		}

		// mandatory field check
		if (StringUtils.isEmpty(productDTO.getName())) {
			throw new InvalidRequestException("Product name is required");
		}

		// mandatory field check
		if (productDTO.getPrice() == null) {
			throw new InvalidRequestException("Product price is required");
		}

		Product product = modelMapper.map(productDTO, Product.class);
		product.setCreatedDate(new Date());
		product.setUpdatedDate(new Date());
		productRepository.save(product);

	}

	@Override
	public void updateProduct(Long id, ProductRequestDTO productDTO) {
		if (id == null) {
			throw new InvalidRequestException("Id is required");
		}

		Product product = productRepository.findById(id)
				.orElseThrow(() -> new NotFoundException("Product with id " + id + " not found"));
		product.setName(productDTO.getName());
		product.setPrice(productDTO.getPrice());
		product.setUpdatedDate(new Date());
		productRepository.save(product);
	}

	@Override
	public void deleteProduct(Long id) {
		Product product = productRepository.findById(id)
				.orElseThrow(() -> new NotFoundException("Product with id " + id + " not found"));
		productRepository.delete(product);
	}

	@Override
	public String getProductByIdEncrypted(Long id) {
		Product product = productRepository.findById(id)
				.orElseThrow(() -> new NotFoundException("Product with id " + id + " not found"));
		ProductResponseDTO productDTO = modelMapper.map(product, ProductResponseDTO.class);
		ObjectMapper objectMapper = new ObjectMapper();
		
		try {
			String json = objectMapper.writeValueAsString(productDTO);
			System.out.println("ResultingJSONstring = " + json);
			
			return AesUtils.encrypt(json, secretKeyValue);
			
		} catch (JsonProcessingException e) {
			throw new RuntimeException("Error prossing result ");
		}
		
	}

	@Override
	public void createProductWithEncrypted(EncryptedRequestDTO encrytedRequest) {
		String requestDataInJsonStr = AesUtils.decrypt(encrytedRequest.getRequestData(), secretKeyValue);
		
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			ProductRequestDTO productRequest = objectMapper.readValue(requestDataInJsonStr, ProductRequestDTO.class);
			Product product = modelMapper.map(productRequest, Product.class);
			product.setCreatedDate(new Date());
			product.setUpdatedDate(new Date());
			productRepository.save(product);
		} catch (JsonMappingException e ) {
			log.error("Error parsing string to json", e);
			throw new RuntimeException("Error prossesing request ");
		} catch (JsonProcessingException e) {
			log.error("Error parsing string to json", e);
			throw new RuntimeException("Error prossesing request ");
		}

	}

}
