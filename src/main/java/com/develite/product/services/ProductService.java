package com.develite.product.services;

import java.util.List;

import com.develite.product.dto.EncryptedRequestDTO;
import com.develite.product.dto.ProductRequestDTO;
import com.develite.product.dto.ProductResponseDTO;

public interface ProductService {

	public List<ProductResponseDTO> getAllProducts();

	public ProductResponseDTO getProductById(Long id);

	public void createProduct(ProductRequestDTO productDTO);

	public void updateProduct(Long id, ProductRequestDTO productDTO);
	
	public void deleteProduct(Long id);
	
	public String getProductByIdEncrypted(Long id);
	
	public void createProductWithEncrypted(EncryptedRequestDTO encrytedRequest);
}
