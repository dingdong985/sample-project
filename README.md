# Test Project

## Projects Content
- "Test Syntax" folder is a java project for Question 2
- "src" folder is the folder for CRUD rest API
- "TestCRUDSample.postman_collection.json" is the postman collection to call API in AWS Cloud
- API Specification can be found here: http://ec2-54-169-103-162.ap-southeast-1.compute.amazonaws.com/product-api/swagger-ui.html#/product-resource

# Nature of the app
The rest API application is build on top of H2 memory database. Data will be reset once the app restarted.

# Pre-requisite:
Java 8

1) To build the package > mvn clean install
2) To run the application:
    > mvn spring-boot:run
3) To run swagger ui
    > http://localhost:20100/product-api/swagger-ui.html